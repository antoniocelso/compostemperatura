## Sobre o projeto  
Iniciando as pesquisas sobre um sistema de aquisição de temperatura de composteiras baseado em licenças livres para uso geral (Pessoal e comercial)

## (Re)Organizando
Organizando via Gitlab usando a interface Jerkyl/Hugo para documentação amigável, a organização do projeto será estabelecida utilizando os Issues e ToDos

## Hugo
https://gohugo.io/getting-started/

## licenças
O Hugo é baseado em Licença MIT possivelmente o projeto do sensor será baseado em licença OHL.
